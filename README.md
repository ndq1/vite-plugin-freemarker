# vite-plugin-freemarker

[Vite](https://github.com/vitejs/vite) plugin for [freemarker](https://freemarker.apache.org/).

This plugin was inspired by [vite-plugin-twig](https://github.com/fiadone/vite-plugin-twig) and [vite-plugin-handlebars](https://github.com/alexlafroscia/vite-plugin-handlebars).

## Installation
```
npm i -D vite-plugin-freemarker
```

## Usage

```js
/* vite.config.js */
import freemarker from 'vite-plugin-freemarker'

export default {
  plugins: [
    freemarker()
  ]
}
```

### Options
The plugin can be configured both via the *freemarker.config.js* file from the project root or by passing a configuration object directly as argument to the function above (in this last case, the configuration file will be ignored).

### Templates
The *html* files located by default in the *Vite* project root are not intented to be replaced directly by the *freemarker* ones as the normal page files resolution/linking on the *Vite*'s dev server is wanted to be preserved along with the build logic. However, those files are supposed to contain a json definition instead of the traditional markup, which should be moved on the *freemarker* side.

More in details, a *html* file should look like this:

```html
<!-- index.html -->
<script type="application/json">
  {
    "template": "path/to/template.ftl",
    "data": {
      "title": "Homepage"
    }
  }
</script>
```

where `template` is the path of the *freemarker* template to be rendered (relative to the *cwd*), and `data` is the local context for that page (eventually merged with the *globals* provided via plugin options).

> ℹ️ The *script* tag is not mandatory, since a plain text representation of the *json* will be correctly parsed too. However, it is recommended for readability and syntax highlighting purposes.

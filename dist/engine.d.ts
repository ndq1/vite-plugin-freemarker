export interface Options {
    root?: string;
    suffix?: string;
    tagSyntax: 'angleBracket' | 'squareBracket' | 'autoDetect';
}
export interface IFMPPInstance {
    tmpDir: string;
    sourceRoot: Options['root'];
    suffix: Options['suffix'];
    tagSyntax: Options['tagSyntax'];
    cmd: string;
    render(str: string, data: Record<string, any>, callback: (err: Error, html: string) => void): void;
    renderFile(file: string, data: Record<string, any>, callback: (err: Error, html: string) => void): Promise<any>;
    renderProxy(file: string, data: Record<string, any>, callback: (err: Error, html: string) => void): void;
}
export declare class TemplateEngine {
    private readonly defaultOptions;
    private readonly options;
    private fmpp;
    constructor(userOptions: Options);
    init(): void;
    render(templatePath: string, data: Record<string, any>): Promise<unknown>;
}

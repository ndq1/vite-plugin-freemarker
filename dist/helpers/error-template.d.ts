export declare const encodeHTMLTags: (err: any) => any;
export declare const styles = "\n  <style>\n    pre {\n      background: #fff78e;\n      padding: 1rem;\n      border: 1px solid #bd7f21;\n      border-radius: 8px;\n      white-space: pre-wrap;\n    }\n  </style>\n";
export declare const renderError: (err: any) => string;

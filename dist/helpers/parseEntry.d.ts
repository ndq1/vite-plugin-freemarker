export declare function parseEntry(content: any): {
    template: string;
    data: any;
} | {
    template?: undefined;
    data?: undefined;
};

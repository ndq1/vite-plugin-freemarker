import { Plugin } from 'vite';
import { Options } from "./engine";
declare const viteFreemarkerPlugin: (_options?: Options) => Plugin;
export default viteFreemarkerPlugin;

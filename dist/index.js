var path = require('path');
var process = require('process');
var Freemarker = require('freemarker');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var Freemarker__default = /*#__PURE__*/_interopDefaultLegacy(Freemarker);

function parseEntry(content) {
  try {
    const [_, specs] = content.match(/<script\b[^>]*>([\s\S]+)<\/script>/) || [];
    const {
      template,
      data
    } = JSON.parse(specs || content);
    return {
      template: path.resolve(process.cwd(), template),
      data
    };
  } catch (err) {
    console.warn(err);
    return {};
  }
}

function retrieveOptions() {
  try {
    const config = path.resolve(process.cwd(), 'freemarker.config.js');
    return require(config);
  } catch (err) {
    return {};
  }
}

const encodeHTMLTags = err => err.replace(/</gm, '&lt;').replace(/>/gm, '&gt;');
const styles = `
  <style>
    pre {
      background: #fff78e;
      padding: 1rem;
      border: 1px solid #bd7f21;
      border-radius: 8px;
      white-space: pre-wrap;
    }
  </style>
`;
const renderError = err => `
  ${styles}
  <pre>${encodeHTMLTags(err)}</pre>
`;

class TemplateEngine {
  constructor(userOptions) {
    this.defaultOptions = void 0;
    this.options = void 0;
    this.fmpp = void 0;
    this.defaultOptions = {
      suffix: 'ftl',
      tagSyntax: 'angleBracket'
    };
    this.options = Object.assign({}, this.defaultOptions, userOptions);
    this.fmpp = null;
  }
  init() {
    this.fmpp = new Freemarker__default["default"](this.options);
  }
  async render(templatePath, data) {
    return new Promise((resolve, reject) => {
      this.fmpp.renderFile(templatePath, data, (err, html) => {
        if (err) {
          reject(err);
          console.log(err);
        }
        resolve(html);
      });
    });
  }
}

const viteFreemarkerPlugin = _options => {
  const options = _options || retrieveOptions();
  const engine = new TemplateEngine(options);
  engine.init();
  return {
    name: 'vite-plugin-freemarker',
    transformIndexHtml: {
      enforce: 'pre',
      async transform(content) {
        const {
          template,
          data
        } = parseEntry(content);
        let result = null;
        try {
          result = template ? await engine.render(template, data) : content;
        } catch (err) {
          result = renderError(err);
        }
        return result;
      }
    },
    handleHotUpdate({
      file,
      server
    }) {
      if (path.extname(file) === '.ftl') {
        server.ws.send({
          type: 'full-reload'
        });
      }
    }
  };
};

module.exports = viteFreemarkerPlugin;
//# sourceMappingURL=index.js.map

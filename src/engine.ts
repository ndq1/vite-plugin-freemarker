import Freemarker from 'freemarker';

export interface Options {
  root?: string;
  suffix?: string;
  tagSyntax: 'angleBracket' | 'squareBracket' | 'autoDetect';
}

export interface IFMPPInstance {
  tmpDir: string;
  sourceRoot: Options['root'];
  suffix: Options['suffix'];
  tagSyntax: Options['tagSyntax'];
  cmd: string;

  render(str: string, data: Record<string, any>, callback: (err: Error, html: string) => void): void;

  renderFile(file: string, data: Record<string, any>, callback: (err: Error, html: string) => void): Promise<any>;

  renderProxy(file: string, data: Record<string, any>, callback: (err: Error, html: string) => void): void;
}

export class TemplateEngine {
  private readonly defaultOptions: { tagSyntax: string; suffix: string };
  private readonly options: { tagSyntax: string; suffix: string };
  private fmpp: IFMPPInstance;

  constructor(userOptions: Options) {
    this.defaultOptions = {
      suffix: 'ftl',
      tagSyntax: 'angleBracket',
    };
    this.options = Object.assign({}, this.defaultOptions, userOptions);
    this.fmpp = null;
  }

  init() {
    this.fmpp = new Freemarker(this.options);
  }

  async render(templatePath: string, data: Record<string, any>) {
    return new Promise((resolve, reject) => {
      this.fmpp.renderFile(templatePath, data, (err, html) => {
        if (err) {
          reject(err)
          console.log(err);
        }
        resolve(html);
      });
    });
  }
}

export const encodeHTMLTags = (err) => err.replace(/</gm, '&lt;').replace(/>/gm, '&gt;');

export const styles = `
  <style>
    pre {
      background: #fff78e;
      padding: 1rem;
      border: 1px solid #bd7f21;
      border-radius: 8px;
      white-space: pre-wrap;
    }
  </style>
`;

export const renderError = (err) => `
  ${styles}
  <pre>${encodeHTMLTags(err)}</pre>
`;

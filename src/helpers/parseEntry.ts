import {resolve} from "path";
import {cwd} from "process";

export function parseEntry(content) {
  try {
    const [_, specs] = content.match(/<script\b[^>]*>([\s\S]+)<\/script>/) || []
    const {template, data} = JSON.parse(specs || content)
    return {template: resolve(cwd(), template), data}
  } catch (err) {
    console.warn(err)
    return {}
  }
}

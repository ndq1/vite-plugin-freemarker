import {resolve} from "path";
import {cwd} from "process";

export function retrieveOptions() {
  try {
    const config = resolve(cwd(), 'freemarker.config.js')
    return require(config)
  } catch (err) {
    return {}
  }
}

import {extname} from 'path';
import {retrieveOptions, parseEntry, renderError} from './helpers';
import {TemplateEngine} from './engine';
import {Plugin} from 'vite';
import {Options} from "./engine";

const viteFreemarkerPlugin = (_options?: Options): Plugin => {
  const options = _options || retrieveOptions();
  const engine = new TemplateEngine(options);

  engine.init();

  return {
    name: 'vite-plugin-freemarker',
    transformIndexHtml: {
      enforce: 'pre',
      async transform(content: string): Promise<string> {
        const {template, data} = parseEntry(content);
        let result = null;

        try {
          result = template ? await engine.render(template, data) : content;
        } catch (err) {
          result = renderError(err);
        }

        return result;
      }
    },
    handleHotUpdate({file, server}) {
      if (extname(file) === '.ftl') {
        server.ws.send({type: 'full-reload'});
      }
    }
  }
};

export default viteFreemarkerPlugin;
